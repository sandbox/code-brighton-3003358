Search API Content Moderation
=============================

Integrates core Content Moderation module with Search API 
(and e.g. with Solr search)

Installing Search API Content Moderation creates a Search API index processor
"Content moderation state" select this on your search index at 
/admin/config/search/search-api/index/INDEX_ID/edit to exclude content with 
moderation states that are not maked as "Published"

