<?php

namespace Drupal\search_api_content_moderation\Plugin\search_api\processor;

use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;

/**
 * Excludes entities with "unpublished" content moderation states.
 *
 * @SearchApiProcessor(
 *   id = "search_api_content_moderation_status",
 *   label = @Translation("Entity content moderation status"),
 *   description = @Translation("Exclude entities with content moderation states that are not marked as 'Published' from being indexed."),
 *   stages = {
 *     "alter_items" = 0,
 *   },
 * )
 */
class EntityContentModerationStatus extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function supportsIndex(IndexInterface $index) {

    foreach ($index->getDatasources() as $datasource) {
      // If any content types attached to this index have a workflow then this
      // processor is valid.
      $entity_type_id = $datasource->getEntityTypeId();
      if (!$entity_type_id) {
        continue;
      }

      $entity_bundles = $datasource->getBundles();
      if (!$entity_bundles) {
        continue;
      }
      // Get the bundles for the entity.
      $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);

      foreach ($entity_bundles as $bundle_id => $bundle_name) {
        if (isset($bundles[$bundle_id]['workflow'])) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {
    foreach ($items as $item_id => $item) {
      $entity = $item->getOriginalObject()->getValue();
      if ($entity->get('moderation_state')) {
        if (!$this->entityInModerationStatePublished($entity)) {
          unset($items[$item_id]);
        }
      }
    }
  }

  /**
   * Check if entity is in a moderation state that is a Published state.
   *
   * @see \Drupal\content_moderation\EntityOperations::isPublished()
   * @see \Drupal\content_moderation\ModerationInformation::getWorkflowForEntity()
   */
  protected function entityInModerationStatePublished($entity) {
    // If the entity implements EntityPublishedInterface directly, check that
    // first, otherwise fall back to check through the workflow state.
    if ($entity instanceof EntityPublishedInterface) {
      return $entity->isPublished();
    }
    if ($moderation_state = $entity->get('moderation_state')->value) {
      $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity->getEntityTypeId());
      if (isset($bundles[$entity->bundle()]['workflow'])) {
        $workflow = \Drupal::entityTypeManager()->getStorage('workflow')->load($bundles[$entity->bundle()]['workflow']);
        return $workflow->getTypePlugin()->getState($moderation_state)->isPublishedState();
      };
    }
    return FALSE;
  }

}
